<?php

require  'vendor/autoload.php';

$config = require_once 'config.php';

try {
    $telegram = new Longman\TelegramBot\Telegram($config['api_key'], $config['bot_username']);
    $result = $telegram->setWebhook($config['webhook']['url']);
    echo $result->getDescription();

} catch (Longman\TelegramBot\Exception\TelegramException $e) {

    echo $e->getMessage();
}