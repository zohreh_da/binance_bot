<?php


return [
    'api_key' => '1730859669:AAGA25uEPf55RgFdJl7gJ82T8e0SSkgLpNU',
    'bot_username' => 'Zohreh99Bot',
    'webhook' => [
        'url' => 'https://xmidrate.com/bott/binance_bot/public/'
    ],

    'commands' => [

        'paths' => [
            __DIR__ . '/src/Commands',
        ],
        'configs' => [

        ],
    ],

    'symbols' => ['BTCUSDT','fff','efe'],

    'binance_api' => [
        'rest' => ['base_url' => 'https://api.binance.com'],
        'websocket' => ['base_url' => 'wss://stream.binance.com:9443/ws/btcusdt@ticker'],
    ],


    'logging' => [
        'debug' => __DIR__ . '/php-telegram-bot-debug.log',
        'error' => __DIR__ . '/php-telegram-bot-error.log',
        'update' => __DIR__ . '/php-telegram-bot-update.log',
    ],


];