<?php

namespace Bot;

use Bot\Commands\ListCommand ;
use Bot\Commands\CallbackqueryCommand ;
use Bot\Commands\SetMinCommand ;
use Bot\Commands\GenericmessageCommand ;

use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Exception\TelegramLogException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\TelegramLog;
use Monolog\Formatter\LineFormatter;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class BinanceBot
{

    private $config;
    private $telegram;


    public function __construct()
    {


        $this->loadDefaultConfig();
    }

    public function run()
    {



        $this->initialize();
    }

    public function loadDefaultConfig()
    {
        $config = require_once '../config.php';
        $this->config = $config;

    }

    /**
     * @return mixed
     */
    public function initialize()
    {

        try {
            // Create Telegram API object
            $telegram = new Telegram($this->config['api_key'], $this->config['bot_username']);
            // Enable admin users
            //$telegram->enableAdmins($config['admins']);


            // Add commands paths containing your custom commands
            //    $telegram->addCommandsPaths($this->config['commands']['paths']);
            $telegram->addCommandClass(ListCommand::class);
            $telegram->addCommandClass(CallbackqueryCommand::class);
            $telegram->addCommandClass(SetMinCommand::class);
            $telegram->addCommandClass(GenericmessageCommand::class);

            $mysql_credentials = [
                'host'     => 'localhost',
                'port'     => 3306, // optional
                'user'     => 'xmidrate_comxb',
                'password' => '123456',
                'database' => 'xmidrate_comxb',
            ];

            // $telegram->enableMySql($mysql_credentials);

            // Enable MySQL if required
            // $telegram->enableMySql($config['mysql']);

            // Logging (Error, Debug and Raw Updates)
            // https://github.com/php-telegram-bot/core/blob/master/doc/01-utils.md#logging
            //
            // (this example requires Monolog: composer require monolog/monolog)

            // Longman\TelegramBot\TelegramLog::initialize(
            //    new Monolog\Logger('telegram_bot', [
            //        (new Monolog\Handler\StreamHandler($config['logging']['debug']", Monolog\Logger::DEBUG))->setFormatter(new Monolog\Formatter\LineFormatter(null, null, true)),
            //        (new Monolog\Handler\StreamHandler($config['logging']['error']", Monolog\Logger::ERROR))->setFormatter(new Monolog\Formatter\LineFormatter(null, null, true)),
            //    ]),
            //    new Monolog\Logger('telegram_bot_updates', [
            //        (new Monolog\Handler\StreamHandler($config['logging']['update']", Monolog\Logger::INFO))->setFormatter(new Monolog\Formatter\LineFormatter('%message%' . PHP_EOL)),
            //    ])
            // );


            TelegramLog::initialize(

                new Logger('telegram_bot',[
                    ( new StreamHandler($this->config['logging']['debug'],Logger::DEBUG ))->setFormatter(new LineFormatter(null, null, true)) ,
                    ( new StreamHandler($this->config['logging']['error'],Logger::ERROR ))->setFormatter(new LineFormatter(null, null, true)) ,

                ]) ,
                new Logger('telegram_bot_updates',[
                    ( new StreamHandler($this->config['logging']['update'],Logger::INFO ))->setFormatter(new LineFormatter('%message%' . PHP_EOL)) ,
                ])
            );

            //    TelegramLog::$always_log_request_and_response = true;
            // Set custom Download and Upload paths
            // $telegram->setDownloadPath($config['paths']['download']);
            // $telegram->setUploadPath($config['paths']['upload']);

            // Load all command-specific configurations
            // foreach ($config['commands']['configs'] as $command_name => $command_config) {
            //     $telegram->setCommandConfig($command_name, $command_config);
            // }

            // Requests Limiter (tries to prevent reaching Telegram API limits)
            //$telegram->enableLimiter($config['limiter']);

            // Handle telegram webhook request
            $telegram->handle();

        } catch (TelegramException $e) {
            // Log telegram errors
            TelegramLog::error($e);

            // Uncomment this to output any errors (ONLY FOR DEVELOPMENT!)
            // echo $e;
        } catch (TelegramLogException $e) {
            // Uncomment this to output log initialisation errors (ONLY FOR DEVELOPMENT!)
            // echo $e;
        }

















        /* $telegram = new Telegram($this->config['api_key'], $this->config['bot_username']);
         $this->telegram = $telegram;
         // var_dump($telegram);
         //    die('ff');
         $this->telegram->addCommandsPaths($this->config['commands']['paths']);

        */


    }


}