<?php

namespace Bot\Commands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\InlineKeyboardButton;

use Longman\TelegramBot\Entities\KeyboardButton;


use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Request;
use Bot\BinanceApi;

class CallbackqueryCommand extends SystemCommand
{

    protected $name = 'callbackquery';


    protected $description = 'Handle the callback query';


    protected $version = '1.2.0';


    public function execute(): ServerResponse
    {
        $callback_query = $this->getCallbackQuery();
        $callback_data = $callback_query->getData();

        //  $user=new UserCommand() ;


        $message = $callback_query->getMessage();

        $chat = $message->getChat();
        $user = $message->getFrom();
        $text = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();


        $callback_data_array = explode('_', $callback_data);
        $type = $callback_data_array[0];
        $obj = $callback_data_array[1];

        if ($type == 'symbol') {
            $api = new BinanceApi();
            $text = $this->symbolInfo($api, $obj);
            $inline_keyboard = [
                new InlineKeyboardButton(
                    [
                        'text' => 'set min',
                        'callback_data' => 'opt_setmin',
                    ]
                ),
                new InlineKeyboardButton(
                    [
                        'text' => 'set max',
                        'callback_data' => 'opt_setmax',
                    ]
                )

            ];

            return Request::editMessageText([
                'chat_id' => $callback_query->getMessage()->getChat()->getId(),
                'message_id' => $callback_query->getMessage()->getMessageId(),
                'text' => $text,
                'reply_markup' => new InlineKeyboard($inline_keyboard),
            ]);
        }
        if ($type == 'opt') {

            return   $this->getTelegram()->executeCommand('setmin');




            /* return $callback_query->answer([
                 'text' => 'fdf ' . $callback_data,
                 //       'show_alert' => (bool) random_int(0, 1), // Randomly show (or not) as an alert.
                 'cache_time' => 5,
             ]);*/
        }
    }

    public function executeCommandd(string $command): ServerResponse
    {
        $command = mb_strtolower($command);

        $command_obj = $this->commands_objects[$command] ?? $this->getCommandObject($command);

        if (!$command_obj || !$command_obj->isEnabled()) {
            //Failsafe in case the Generic command can't be found
            if ($command === self::GENERIC_COMMAND) {
                throw new TelegramException('Generic command missing!');
            }

            //Handle a generic command or non existing one
            $this->last_command_response = $this->executeCommand(self::GENERIC_COMMAND);
        } else {
            //execute() method is executed after preExecute()
            //This is to prevent executing a DB query without a valid connection
            $this->last_command_response = $command_obj->preExecute();
        }

        return $this->last_command_response;
    }


    public function symbolInfo($app, $symbol)
    {
        $ticker = $app->ticker($symbol);

        $str = strtoupper($symbol) . PHP_EOL .
            'low price:' . $ticker->getLowPrice() . PHP_EOL .
            'high price:' . $ticker->getHighPrice() . PHP_EOL .
            'high price:' . $ticker->getLastPrice() . PHP_EOL .
            'ask price:' . $ticker->getAskPrice();

        return $str;

    }

    public function getTicker($symbol)
    {
        // return 'er' ;
        new BinanceApi();
        //    $rr = $api->ticker('BTCUSDT')->getLowPrice();

        //     return $rr;

    }

    public function fdfdg($symbol)
    {
        try {

            $api = new BinanceApi();
            $rr = $api->ticker('BTCUSDT')->getLowPrice();

            return $rr;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }


    }


}
