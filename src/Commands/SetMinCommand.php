<?php

namespace Bot\Commands ;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\InlineKeyboardButton;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\InlineKeyboard;

use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Exception\TelegramLogException;
use Longman\TelegramBot\TelegramLog;
use Monolog\Formatter\LineFormatter;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Bot\BinanceApi;

class SetMinCommand extends UserCommand
{
    protected $name = 'setmin';
    protected $description = 'list all crypto';
    protected $usage = '/setmin';
    protected $version = '1.0.0';

    public function execute(): ServerResponse
    {
        if($this->getCallbackQuery()){
            return $this->executeCallback();
        }
        return $this->executeMessage();
    }

    public function executeCallback()
    {
        // Explode query_data into chunks
        $callback_query = $this->getUpdate()->getCallbackQuery() ;

        $query_data     = $callback_query->getData();
        //  $query_data     = explode(':', $query_data);

        $message = $callback_query->getMessage();

        $chat = $message->getChat();
        $user = $message->getFrom();
        $text = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();















        $data = [
            'chat_id'      => $chat_id,
            // Remove any keyboard by default
            'reply_markup' => Keyboard::remove(['selective' => true]),
        ];

        if ($chat->isGroupChat() || $chat->isSuperGroup()) {
            // Force reply is applied by default so it can work with privacy on
            $data['reply_markup'] = Keyboard::forceReply(['selective' => true]);
        }

        // Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());

        // Load any existing notes from this conversation
        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];
        $file='filee.txt' ;
        file_put_contents($file,'fff'.PHP_EOL) ;

        // Load the current state of the conversation
        $state = $notes['state'] ?? 0;

        $result = Request::emptyResponse();

        // State machine
        // Every time a step is achieved the state is updated
        /* switch ($state) {
             case 0:
                 if ($text === '') {
                     $notes['state'] = 0;
                     $this->conversation->update();

                     $data['text'] = 'Type your name:';

                     $result = Request::sendMessage($data);
                     break;
                 }

                 $notes['name'] = $text;
                 $text          = '';

             // No break!
             case 1:
                 if ($text === '') {
                     $notes['state'] = 1;
                     $this->conversation->update();

                     $data['text'] = 'Type your surname:';

                     $result = Request::sendMessage($data);
                     break;
                 }

                 $notes['surname'] = $text;
                 $text             = '';

             // No break!
             case 2:
                 if ($text === '' || !is_numeric($text)) {
                     $notes['state'] = 2;
                     $this->conversation->update();

                     $data['text'] = 'Type your age:';
                     if ($text !== '') {
                         $data['text'] = 'Age must be a number';
                     }

                     $result = Request::sendMessage($data);
                     break;
                 }

                 $notes['age'] = $text;
                 $text         = '';

             // No break!
             case 3:
                 if ($text === '' || !in_array($text, ['M', 'F'], true)) {
                     $notes['state'] = 3;
                     $this->conversation->update();

                     $data['reply_markup'] = (new Keyboard(['M', 'F']))
                         ->setResizeKeyboard(true)
                         ->setOneTimeKeyboard(true)
                         ->setSelective(true);

                     $data['text'] = 'Select your gender:';
                     if ($text !== '') {
                         $data['text'] = 'Choose a keyboard option to select your gender';
                     }

                     $result = Request::sendMessage($data);
                     break;
                 }

                 $notes['gender'] = $text;

             // No break!
             case 4:
                 if ($message->getLocation() === null) {
                     $notes['state'] = 4;
                     $this->conversation->update();

                     $data['reply_markup'] = (new Keyboard(
                         (new KeyboardButton('Share Location'))->setRequestLocation(true)
                     ))
                         ->setOneTimeKeyboard(true)
                         ->setResizeKeyboard(true)
                         ->setSelective(true);

                     $data['text'] = 'Share your location:';

                     $result = Request::sendMessage($data);
                     break;
                 }

                 $notes['longitude'] = $message->getLocation()->getLongitude();
                 $notes['latitude']  = $message->getLocation()->getLatitude();

             // No break!
             case 5:
                 if ($message->getPhoto() === null) {
                     $notes['state'] = 5;
                     $this->conversation->update();

                     $data['text'] = 'Insert your picture:';

                     $result = Request::sendMessage($data);
                     break;
                 }

                 $photo             = $message->getPhoto()[0];
                 $notes['photo_id'] = $photo->getFileId();

             // No break!
             case 6:
                 if ($message->getContact() === null) {
                     $notes['state'] = 6;
                     $this->conversation->update();

                     $data['reply_markup'] = (new Keyboard(
                         (new KeyboardButton('Share Contact'))->setRequestContact(true)
                     ))
                         ->setOneTimeKeyboard(true)
                         ->setResizeKeyboard(true)
                         ->setSelective(true);

                     $data['text'] = 'Share your contact information:';

                     $result = Request::sendMessage($data);
                     break;
                 }

                 $notes['phone_number'] = $message->getContact()->getPhoneNumber();

             // No break!
             case 7:
                 $this->conversation->update();
                 $out_text = '/Survey result:' . PHP_EOL;
                 unset($notes['state']);
                 foreach ($notes as $k => $v) {
                     $out_text .= PHP_EOL . ucfirst($k) . ': ' . $v;
                 }

                 $data['photo']   = $notes['photo_id'];
                 $data['caption'] = $out_text;

                 $this->conversation->stop();

                 $result = Request::sendPhoto($data);
                 break;
         }*/

        return $result;





    }

}