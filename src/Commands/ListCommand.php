<?php

namespace Bot\Commands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\InlineKeyboardButton;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\InlineKeyboard;

use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Exception\TelegramLogException;
use Longman\TelegramBot\TelegramLog;
use Monolog\Formatter\LineFormatter;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Bot\BinanceApi;


class ListCommand extends UserCommand
{
    protected $name = 'list';                      // Your command's name
    protected $description = 'list all crypto'; // Your command description
    protected $usage = '/list';                    // Usage of your command
    protected $version = '1.0.0';                  // Version of your command

    public function execute(): ServerResponse
    {
        $message = $this->getMessage();           // Get Message object
        $chat_id = $message->getChat()->getId();   // Get the current Chat ID

        $api = new BinanceApi();
        $symbols_list_array = $api->symbolsList();
        $re = '';
        $button=[] ;
        foreach ($symbols_list_array as $key => $symbol) {
            $re .= $key + 1 . ')' . $symbol . PHP_EOL;
            $button[] = new InlineKeyboardButton(
                [
                    'text' => $symbol,
                    'callback_data' => 'symbol_'.$symbol,
                ]
            );


        }
        //   $symbols_list_string = implode(',', $symbols_list_array);

        $inline_keyboard=$button ;

        /*   $inline_keyboard = [
               new InlineKeyboardButton(
                   [
                       'text' => 'BTCUSDT',
                       'callback_data' => 'symbol_BTCUSDT',
                   ]
               )
           ];*/


        $data = [                                  // Set up the new message data
            'chat_id' => $chat_id,                 // Set Chat ID to send the message to
            'text' => $re, // Set message to send
            'reply_markup' => new InlineKeyboard($inline_keyboard),
        ];

        $result = Request::sendMessage($data);


        /*  TelegramLog::initialize(

              new Logger('telegram_bot',[
                  ( new StreamHandler($this->config['logging']['debug'],Logger::DEBUG ))->setFormatter(new LineFormatter(null, null, true)) ,
                  ( new StreamHandler($this->config['logging']['error'],Logger::ERROR ))->setFormatter(new LineFormatter(null, null, true)) ,

              ]) ,
              new Logger('telegram_bot_updates',[
                  ( new StreamHandler($this->config['logging']['update'],Logger::INFO ))->setFormatter(new LineFormatter('%message%' . PHP_EOL)) ,
              ])
          );
  */


        return $result;       // Send message!
    }
}