<?php

namespace Bot;

use GuzzleHttp\Client;

use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Exception\TelegramLogException;
use Longman\TelegramBot\TelegramLog;
use Monolog\Formatter\LineFormatter;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

define("ROOT_PATH", realpath(dirname(__DIR__)));
//define("APP_PATH", ROOT_PATH . '/bot');
define("SRC_PATH", ROOT_PATH . '/src');

class BinanceApi
{
    private $config;
    private $client;
    private $result;

    public function __construct()
    {
        $this->client = new Client();

        $this->loadDefaultConfig();
    }

    public function loadDefaultConfig()
    {
        $config = require_once 'config.php';
        $this->config = $config;

    }

    public function symbolsList()
    {


        // return ['BTCUSDT', $a, 'efe'];
        return $this->config['symbols'];

    }

    public function ticker($symbol)
    {
        // return 'g' ;

        // https://api.binance.com/api/v3/ticker/24hr?symbol=BTCUSDT

        $endpoint = '/api/v3/ticker/24hr';
        $client = $this->client;
        $url = 'https://api.binance.com' . $endpoint;
        //  $url =$this->config['binance_api']['rest']['base_url'] . $endpoint;
        $response = $client->request('get', $url, [
            'query' => ['symbol' => $symbol]]);

        $this->result = json_decode($response->getBody());
        return $this;
    }

    public function getBody()
    {
        return $this->result;
    }

    public function getLowPrice()
    {
        return $this->result->lowPrice;
    }

    public function getHighPrice()
    {
        return $this->result->highPrice;
    }

    public function getLastPrice()
    {
        return $this->result->lastPrice;
    }

    public function getAskPrice()
    {
        return $this->result->askPrice;
    }

    public function __destruct()
    {
        TelegramLog::initialize(

            new Logger('telegram_bot', [
                (new StreamHandler($this->config['logging']['debug'], Logger::DEBUG))->setFormatter(new LineFormatter(null, null, true)),
                (new StreamHandler($this->config['logging']['error'], Logger::ERROR))->setFormatter(new LineFormatter(null, null, true)),

            ]),
            new Logger('telegram_bot_updates', [
                (new StreamHandler($this->config['logging']['update'], Logger::INFO))->setFormatter(new LineFormatter('%message%' . PHP_EOL)),
            ])
        );
    }


}




